//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by Robert Szost on 01.11.2017.
//  Copyright © 2017 Robert Szost. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ballAnswer: UIImageView!
    
    var ballArray : Array = ["ball1","ball2","ball3","ball4","ball5"]
    var randomBallNumber : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeBall()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func askButtonPressed(_ sender: UIButton) {
        changeBall()
    }
    
    func changeBall(){
        randomBallNumber = Int(arc4random_uniform(4))
        ballAnswer.image = UIImage(named: ballArray[randomBallNumber])
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        randomBallNumber = Int(arc4random_uniform(4))
        ballAnswer.image = UIImage(named: ballArray[randomBallNumber])
    }
}

